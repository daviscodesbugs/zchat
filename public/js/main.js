var app = angular.module("zchat", []);

app.controller("mainCtrl", function($scope, $http, $location) {
    $scope.peer = null;
    $scope.peers = [];
    $scope.message = null;
    $scope.messages = [];

	var socket = io();
	socket.on('connected', function (data) {
		console.log(data);
	});
	socket.on('notify', function (message) {
		console.log("Notify:", message);
	});
	socket.on('message', function (message) {
		$scope.messages.push(message);
		$scope.$apply();
	});
	socket.on('peer', function () {
		getPeers();
	});
	socket.on('giphy', function (message) {

		$scope.messages.push(message);
		$scope.$apply();
	});

    $scope.sendMessage = function() {
        if ($scope.message.trim().length > 0) {
            var msg = {
                source: $location.absUrl().split('?')[0],
                time: Date.now(),
                sender: $scope.name,
                message: $scope.message
            };
            socket.emit('message', msg);
            $scope.message = null;
            document.getElementById("msg-input").focus();
        }
    }
    $scope.sendPeer = function() {
        if ($scope.peer != null && $scope.peer.trim().length > 0) {
            var peer = {
                peer: $scope.peer
            };
            socket.emit('peer', peer);
            $scope.peer = null;
        } else {
            $scope.peer = null;
        }
    };

    function getPeers() {
        $http.get('/api/peer')
            .then(function(res) {
                $scope.peers = res.data;
            }, function(err) {
                console.log("Failed to get peers.");
            });
    }

    function getMessages() {
        $http.get('/api/message')
            .then(function(res) {
                $scope.messages = res.data;
            }, function(err) {
                console.log("Failed to get peers.");
            });
    }

	$scope.uploadFile = function(files) {
		let fd = new FormData();
		//Take the first selected file
		fd.append("file", files[0]);
		let reader = new FileReader();
		reader.onload = function (loadEvent) {
			let fileData = loadEvent.target.result;
			
			var msg = {
                source: $location.absUrl().split('?')[0],
				time: Date.now(),
				sender: $scope.name,
				message: fileData,
				isFile: true
			};
			socket.emit('message', msg);
		};
		reader.readAsDataURL(files[0]);

	};

    getPeers();
    getMessages();
    document.getElementById("msg-input").focus();
});
