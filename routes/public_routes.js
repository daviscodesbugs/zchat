var routes = [
    {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'public',
                index: true
            }
        }
    }
];
module.exports = routes;
