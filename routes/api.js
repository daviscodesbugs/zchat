const fs = require('fs');
const request = require('request');
const async = require('async');
const pad = require('pad');
const uuidV4 = require('uuid/v4');
const imgur = require('imgur');

var messages = {};

var peers = {};
var uuid = undefined;
var messageCount = 0;

loadLocalFiles();

var routes = [{
        method: 'GET',
        path: '/heartbeat',
        handler: function(req, reply) {
            return reply("UP");
        }
    },
    {
        method: 'GET',
        path: '/api/message',
        handler: function(req, reply) {
            let requestedUUID = req.query.uuid;
            let wantCount = req.query.count;
            let mid = req.query.mid;

            if (wantCount) {
                return reply(messageCount);
            } else if (requestedUUID) {
                if (mid) {
                    var requestedMessage = messages[requestedUUID]
                        .find(function(message) { return message.messageID == parseInt(mid); });
                    console.log("Requested message:", requestedMessage);

                    if (requestedMessage == undefined) {
                        requestMessage(requestedUUID, mid);
                    }

                    return reply(requestedMessage);
                } else {
                    return reply(messages[requestedUUID]);
                }
            } else {
                let allMessages = [];
                for (var key in messages) {
                    if (messages.hasOwnProperty(key)) {
                        allMessages.concat(messages[key]);
                    }
                }
                return reply(allMessages);
            }
        }
    },
    {
        method: 'GET',
        path: '/api/peer',
        handler: function(req, reply) {
            return reply(peers);
        }
    },
    {
        method: 'POST',
        path: '/api/message',
        handler: function(req, reply) {
            // save message
            var message = req.payload;
            if (!peers[message.source]) {
                functions.addPeer({ peer: message.source }, function() {
                    console.log("Received message:", message);
                    functions.addMessage(message);
                    if (missingMessages(message.UUID, message.messageID)) {
                        requestMissingHistory(message.UUID, message.messageID, message.source);
                    }
                    return reply('Success');
                });
            } else {
                console.log("Received message:", message);
                functions.addMessage(message);
                if (missingMessages(message.UUID, message.messageID)) {
                    requestMissingHistory(message.UUID, message.messageID, message.source);
                }
                return reply('Success');
            }
        }
    },
    {
        method: 'POST',
        path: '/api/peer',
        handler: function(req, reply) {
            // save peer
            console.log("Received peer:", req.payload);
            functions.addPeer(req.payload, function(result) {
                return reply(result);
            });
        }
    }
];

var functions = {
    getMessages: function() {
        return messages;
    },
    getMessageCount: function() {
        return messageCount;
    },
    addMessage: function(message, fromBrowser) {
        if (fromBrowser) {
            message.UUID = uuid;
            message.messageID = messageCount;
            messageCount += 1;
            fs.writeFile("rsrc/count.txt", messageCount, function(err) {
                if (err) { console.log("write count to disk error"); }
            });

            handleGiphy(message, function(message) {
                handleFile(message, function(message) {
                    propegateMessage(message);
                });
            });
        } else {
            propegateMessage(message);
        }
    },
    addPeer: function(peer, cb) {
        // set peers fail count to 0
        console.log("saving peer: " + peer.peer);
        peers[peer.peer] = 0;

        fs.writeFile("rsrc/peers.txt", JSON.stringify(peers), function(err) {
            if (err) { cb(err); }
            functions.gotNewPeer();
            cb('Success');
        });
    },
    gotNewMessage: function(message) {
        return;
    },
    gotNewPeer: function() {
        return;
    }
}

module.exports = {
    routes: routes,
    functions: functions
}


/**
 * Every minute, check peer list ot see if they're still up
 * If a peer is down for 10 minutes, forget them as a peer.
 */
function verifyPeers() {
    setInterval(function() {
        Object.keys(peers).forEach(function(peer) {
            if (peers[peer] >= 10) {
                delete peers[peer];
            }
        });
        if (Object.keys(peers).length > 0) {
            console.log("Checking peers...");
            async.each(Object.keys(peers), function(peer, cb) {
                var checkURL = peer + "/heartbeat"
                request(checkURL, function(err, res, data) {
                    if (data != "UP") {
                        peers[peer] += 1;
                        console.log("\t", peer, "is DOWN", peers[peer]);
                    } else {
                        peers[peer] = 0;
                    }
                    cb();
                });
            }, function(err) {
                if (err) { console.log("upCheck failed"); }
                fs.writeFile("rsrc/peers.txt", JSON.stringify(peers), function(err) {
                    if (err) { return reply(err); }
                    console.log("Updated peers.");
                });
            });
        }
    }, 60000);
};

/**
 * Send the messages to all peers that you know about.
 */
function propegateMessage(message) {
    console.log("message sending: ", message);
    if (!messages[message.UUID]) {
        messages[message.UUID] = [];
    }
    let messageList = messages[message.UUID];
    if (messageList[messageList.length - 1] && messageList[messageList.length - 1].messageID >= message.messageID) {
        console.log("duplicate message");
        return;
    }
    messages[message.UUID].push(message);

    if (Object.keys(peers).length > 0) {
        var propegations = [];

        async.each(Object.keys(peers), function(peer, next) {
            var options = {
                method: 'POST',
                url: peer + "/api/message",
                json: true,
                body: message
            }
            request(options, (err, res, body) => {
                if (err)
                    propegations.push("  " + pad(peer, 35) + "\tFAILURE");
                else
                    propegations.push("  " + pad(peer, 35) + "\tSUCCESS");
                next();
            });
        }, (err) => {
            if (err) console.log("ERROR propegating messages");
            console.log("Message propegated:");
            propegations.forEach(function(p) {
                console.log(p);
            });
        });
    }

    functions.gotNewMessage(message);
}

/**
 * 1. Remove the "/giphy" keyword just leaving the search terms.
 * 2. get GIPHY url from GIPHY server
 * 3. save url as message of the message
 */
function handleGiphy(message, cb) {
    message.isGiphy = false;
    if (message.message.split(" ")[0].toLowerCase() === "/giphy") {
        var temp = message.message.split(' ');
        temp.shift();
        var searchPhrase = temp.join(' ');
        var searchURL = 'https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=' + searchPhrase;

        request(searchURL, (err, res, data) => {
            if (err) {
                console.log("Error getting giphy:", err);
                message.message = "Unable to get GIF";
            } else {
                message.isGiphy = true;
                message.message = JSON.parse(data).data.image_url.replace(/http/g, "https");
            }
            cb(message);
        });
    } else {
        cb(message);
    }
}

/**
 * Takes a base64 encoded image, uploads it to imgur, and sets the message
 * to be the URL of the image to load.
 */
function handleFile(message, cb) {
    if (message.isFile) {
        imgur.uploadBase64(message.message.split(",")[1])
            .then(function(json) {
                console.log("Uploaded to imgur:", json.data.link);
                message.isGiphy = true;
                message.message = json.data.link.replace(/http/g, "https");
                cb(message);
            })
            .catch(function(err) {
                message.message = "Error uploading file to Imgur";
                cb(message);
            });
    } else {
        cb(message);
    }
}

function loadLocalFiles() {
    // load peers from disk
    if (fs.existsSync('rsrc/peers.txt')) {
        fs.readFile('rsrc/peers.txt', 'utf8', function(err, data) {
            if (err) { throw err; }
            peers = JSON.parse(data);
            console.log("Peers loaded");
            console.log(JSON.stringify(peers, null, 2));
            verifyPeers();
        });
    } else {
        fs.writeFile("rsrc/peers.txt", JSON.stringify(peers), (err) => {
            if (err) { throw err; }
            console.log("Peer file generated");
            verifyPeers();
        });
    }

    // get/generate uuid
    if (fs.existsSync('rsrc/uuid.txt')) {
        fs.readFile('rsrc/uuid.txt', 'utf8', function(err, data) {
            if (err) { throw err; }
            uuid = data;
            console.log("UUID loaded:", uuid);
        });
    } else {
        uuid = uuidV4();
        fs.writeFile("rsrc/uuid.txt", uuid, (err) => {
            if (err) { throw err; }
            console.log("UUID generated:", uuid);
        });
    }

    // load message count from disk
    if (fs.existsSync('rsrc/count.txt')) {
        fs.readFile('rsrc/count.txt', 'utf8', function(err, data) {
            if (err) { throw err; }
            messageCount = parseInt(data);
            console.log("Message count loaded:", messageCount);
        });
    } else {
        fs.writeFile("rsrc/count.txt", messageCount, (err) => {
            if (err) { throw err; }
            console.log("Message count file generated:", messageCount);
        });
    }
}

var randomProperty = function(obj) {
    var keys = Object.keys(obj)
    return obj[keys[keys.length * Math.random() << 0]];
};

function missingMessages(uuid, mid) {
    // new person
    if (!messages[uuid]) {
        if (mid == 0) return false;
        else return true;
    } else {
        var mostRecentMessage = Math.max.apply(Math, messages[uuid].map(function(i) { return i.messageID; }));
        if (mostRecentMessage + 1 == mid) return false;
        else return true;
    }
    throw "Shouldn't ever get here."
}

function requestMissingHistory(uuid, mid, source) {
    let ordered = messages[uuid].sort(function(a, b) {
        return a.messageID - b.messageID;
    });

    let missing = [];
    let val = 0;
    for (var i = 0; i < ordered.length; i++) {
        if (ordered[i].messageID != val) {
            missing.push(val);
            i--;
        }
        val++;
    }

    console.log("Missing message IDs", missing);
    missing.forEach(function(mid) {
        // request mid from peers
        requestMessage(uuid, mid, source);
    });

    return missing;
}

function requestMessage(uuid, mid, source) {
    request(source + "/api/message?uuid=" + uuid + "&mid=" + mid, (err, res, body) => {
        if (err) console.log(err.message);
        else {
            console.log("Got missed message:", body);
            functions.addMessage(body);
        }
    });
};