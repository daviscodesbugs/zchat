const assert = require('assert');
const portfinder = require('portfinder');
const hapi = require('hapi');
const localtunnel = require('localtunnel');

// Get an available port to serve on
portfinder.getPort(function(err, port) {
    'use strict';
    const server = new hapi.Server();
    server.connection({
        port: port,
        host: 'localhost'
    });

    server.register(require('inert'), (err) => {
        if (err) { throw err; }

        // Load in routes
        var api = require('./routes/api');
        var public_routes = require('./routes/public_routes');
        server.route(api.routes);
        server.route(public_routes);

        // Set up socket messages
        const io = require('socket.io')(server.listener);
        io.on('connection', function(socket) {
            console.log("Socket connected");
            socket.emit('connected', "Socket connected.");

            socket.on('message', function(message) {
                api.functions.addMessage(message, true);
            });
            socket.on('peer', function(peer) {
                api.functions.addPeer(peer, function() {});
            });

			api.functions.gotNewMessage = function (message) {
				if(!message.hasOwnProperty('isGiphy')){
					socket.emit('message',message);
				}
				if(message.isGiphy){
					socket.emit('giphy',message);
				} else {
					socket.emit('message', message);
				}
			}
			api.functions.gotNewPeer = function () {
				socket.emit('peer');
			}
		});

        server.start((err) => {
            if (err) { throw err; }
            console.log("Server running at", server.info.uri);

            /*
            Sometimes localtunnel just crashes, so this is a workaround
            to reconnect on the same subdomain
            */
            var reconnecting = true;
            var lt = null;
            var options = {
                subdomain: null
            }

            function digATunnel() {
                lt = localtunnel(port, options, function(err, tunnel) {
                    if (!err) {
                        console.log("Opened up to public at", tunnel.url);
                        options.subdomain = tunnel.tunnel_cluster._opt.name;
                        reconnecting = false;
                    } else {
                        digATunnel();
                    }
                });

                lt.on('close', function() {
                    console.log("Tunnel closed. No longer open to public.");
                });

                lt.on('error', function(err) {
                    console.log("ERROR (localtunnel):", err);

                    if (!reconnecting) {
                        reconnecting = true;
                        console.log("Attempting to relaunch with subdomain", options.subdomain);
                        digATunnel();
                    }
                });
            }

            digATunnel();
        });
    });
});