# Zombie Chat
A decentralized chat client using an epidemic algorithm

## Getting Started
Install Node  
`sudo apt-get install npm nodejs -y`

Check the version with `node -v`  
If the `node` command still isn't recognized, link it in with the following command:  
`which nodejs | xargs -I{} sudo ln -s {} /usr/local/bin/node`

Clone the repository  
`git clone <repo url>`

Setup the project  
`cd zchat`  
`npm install`

Run the project  
`npm start`

Access the app at the url "Opened up to the public at _<url\>_"

## Reference

Messages are structured as follows

```
{
  "id": "23084760-ff5f-49bf-8c40-1423ba00fd98:12",
  "time": 1492274183101,
  "sender": "Bill Murray",
  "message": "I know that's not your middle name",
  "isGiphy": false
}
```